-- Your SQL goes here
CREATE TABLE `plots` (
	`id`	TEXT NOT NULL UNIQUE,
	`name`	TEXT NOT NULL,
	`unit`	TEXT NOT NULL,
	`kind`	TEXT NOT NULL,
	PRIMARY KEY(`id`)
) WITHOUT ROWID;