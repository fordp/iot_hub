use crate::schema::plots;

use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum Kind {
	Line,
	Bar,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DataPair {
	pub value: usize,
	pub time: usize,
}

#[derive(Debug, Insertable, Queryable, Serialize, Deserialize)]
pub struct Plot {
	pub id: String,
	pub name: String,
	pub unit: String,
	pub kind: String,
	// pub data: Vec<DataPair>,
}
