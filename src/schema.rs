table! {
    plots (id) {
        id -> Text,
        name -> Text,
        unit -> Text,
        kind -> Text,
    }
}
