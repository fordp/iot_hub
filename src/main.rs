#![feature(uniform_paths)]
#![allow(proc_macro_derive_resolution_fallback)]

#[macro_use]
extern crate diesel;

use actix_web::{middleware, server, App};

use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;
use dotenv::dotenv;

mod models;
mod schema;

mod api;

fn main() {
	std::env::set_var("RUST_LOG", "actix_web=info,iot_hub=info");
	pretty_env_logger::init();

	let server = server::new(|| {
		App::new()
			.middleware(middleware::Logger::new("%r -> %s"))
			.resource("/new", |r| r.post().with(api::new))
			.resource("/get/{id}", |r| r.get().with(api::get))
			.resource("/delete/{id}", |r| r.delete().with(api::delete))
			.resource("/update", |r| r.post().with(api::update))
		// .resource("/socket/{id}", |r| r.get().with(new::handler))
	});

	server
		.bind("127.0.0.1:80")
		// .bind("192.168.1.110:80")
		.expect("Can't bind to port 80")
		.run();
}

fn establish_connection() -> SqliteConnection {
	dotenv().ok();

	let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
	SqliteConnection::establish(&database_url)
		.expect(&format!("Error connecting to {}", database_url))
}
