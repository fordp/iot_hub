use log::info;

use actix_web::{HttpRequest, HttpResponse};

use diesel::prelude::*;

pub fn handler(req: HttpRequest) -> HttpResponse {
	use crate::schema::plots::dsl::*;

	let u_id = match req.match_info().get("id") {
		Some(u_id) => u_id,
		None => return HttpResponse::BadRequest().finish(),
	};

	let conn = crate::establish_connection();

	match diesel::delete(plots.find(u_id)).execute(&conn) {
		Ok(0) => HttpResponse::NotFound().finish(),
		Ok(1) => {
			info!("Deleted plot with id '{}'", u_id);
			HttpResponse::NoContent().finish()
		}
		Ok(_) => unreachable!(),
		Err(e) => HttpResponse::InternalServerError().body(format!("{}", e)),
	}
}
