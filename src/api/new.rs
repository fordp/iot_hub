use crate::models::{DataPair, Kind, Plot};

use log::info;

use actix_web::http::header;
use actix_web::{HttpResponse, Json};

use diesel::dsl::*;
use diesel::prelude::*;

use serde::Deserialize;

#[derive(Deserialize)]
pub struct NewPlot {
	name: String,
	unit: String,
	kind: Kind,
}

pub fn handler(json_plot: Json<NewPlot>) -> HttpResponse {
	use crate::schema::plots::dsl::*;

	let plot = Plot {
		id: uuid::Uuid::new_v4().to_string(),
		unit: json_plot.unit.clone(),
		kind: format!("{:?}", json_plot.kind),
		name: json_plot.name.clone(),
		// data: vec![
		// 	DataPair { time: 0, value: 10 },
		// 	DataPair { time: 1, value: 11 },
		// ],
	};

	let conn = crate::establish_connection();

	match insert_into(plots).values(&plot).execute(&conn) {
		Ok(_) => (),
		Err(e) => {
			return HttpResponse::InternalServerError().body("Failed to insert Plot");
		}
	};

	match serde_json::to_string(&plot) {
		Ok(t) => {
			info!("Created Plot with id '{}'", plot.id);
			HttpResponse::Created()
				.header(header::CONTENT_TYPE, "application/json")
				.body(t)
		}
		Err(_) => HttpResponse::InternalServerError().body("Failed to deserialize Plot"),
	}
}
