mod delete;
mod get;
mod new;
mod update;

pub use delete::handler as delete;
pub use get::handler as get;
pub use new::handler as new;
pub use update::handler as update;
