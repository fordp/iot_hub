use crate::models::DataPair;

use log::info;

use actix_web::{HttpResponse, Json};

use serde::Deserialize;
use uuid::Uuid;

#[derive(Deserialize)]
pub struct Update {
	data: DataPair,
	id: Uuid,
}

pub fn handler(update: Json<Update>) -> HttpResponse {
	const SUCCESFUL: bool = true;
	const DOES_EXIST: bool = true;

	if DOES_EXIST {
		if SUCCESFUL {
			info!("Updated Plot with id '{}'", update.id);
			return HttpResponse::NoContent().finish();
		} else {
			return HttpResponse::InternalServerError().finish();
		}
	} else {
		return HttpResponse::NotFound().finish();
	}
}
