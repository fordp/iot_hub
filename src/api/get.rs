use crate::models::Plot;

use log::info;

use actix_web::http::header;
use actix_web::{HttpRequest, HttpResponse};

use diesel::prelude::*;
use diesel::result::Error::*;

pub fn handler(req: HttpRequest) -> HttpResponse {
	use crate::schema::plots::dsl::*;

	let u_id = match req.match_info().get("id") {
		Some(u_id) => u_id,
		None => return HttpResponse::BadRequest().finish(),
	};

	let conn = crate::establish_connection();

	let plot = match plots.find(u_id).first::<Plot>(&conn) {
		Ok(plot) => plot,
		Err(NotFound) => return HttpResponse::NotFound().finish(),
		_ => return HttpResponse::InternalServerError().finish(),
	};

	match serde_json::to_string(&plot) {
		Ok(t) => {
			info!("Retrieved Plot with id '{}'", u_id);
			HttpResponse::Ok()
				.header(header::CONTENT_TYPE, "application/json")
				.body(t)
		}
		Err(_) => HttpResponse::InternalServerError().body("Failed to deserialize Plot"),
	}
}
